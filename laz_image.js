document.addEventListener('DOMContentLoaded', function(){
	document.querySelectorAll('img').forEach(function(img){

		img.classList.add('lazy');

		if( img.src && !img.dataset.src ){
			var st = img.style.display;
			img.dataset.src = img.src;
			img.src = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';

			if( img.srcset ){
				img.dataset.srcset = img.srcset
				img.srcset = '';
			}
		}
	});
	(function( d ){
		var imgs = [].slice.call( document.querySelectorAll("img.lazy") ),
			active = false;

		var lazyLoad = function() {
			if (active === false) {
				active = true;

				setTimeout(function() {
					imgs.forEach(function(img) {
						if( ( img.getBoundingClientRect().top <= window.innerHeight && img.getBoundingClientRect().bottom >= 0 ) && getComputedStyle(img).display !== "none") {
							img.src = img.dataset.src;
							if( img.dataset.srcset ){
								img.srcset = img.dataset.srcset;
							}
							img.classList.remove("lazy");

							      imgs = imgs.filter(function(image) {
								return image !== img;
							});
							if (imgs.length === 0) {
								document.removeEventListener("scroll", lazyLoad);
								window.removeEventListener("resize", lazyLoad);
								window.removeEventListener("orientationchange", lazyLoad);
							}
						}
					});
					active = false;
				}, 200);
			}
		};

		document.addEventListener("scroll", lazyLoad);
		window.addEventListener("resize", lazyLoad);
		window.addEventListener("orientationchange", lazyLoad);
		window.addEventListener("load", lazyLoad);
		setTimeout( function(){
			window.removeEventListener("load", lazyLoad);
		}, 1000 );

	})( document );
});